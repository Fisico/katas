package kata;

public class Greeter {
    Console console;

    public Greeter(Console console) {
        this.console = console;
    }

    public void greet(String name) {
        String msg = generateGreetString(name);
        console.print(msg);
    }

    private String generateGreetString(String name) {
        if (name == null) {
            return  "Hello, my friend!";
        }

        String msg = "Hello, " + name + "!";
        if(isUppercase(name)) {
          msg =  msg.toUpperCase();
        }
        return msg;
    }

    private boolean isUppercase(String name) {
        return name.toUpperCase().equals(name);
    }

    public void greet(String[] myNames) {
        String nameComposed = composeNames(myNames);
        greet(nameComposed);
    }

    private String composeNames(String[] myNames) {
        StringBuilder sb = new StringBuilder();
        for(int index = 0; index < myNames.length; index++) {
            sb.append(myNames[index]);
            if(index < myNames.length - 1) {
                sb.append(", ");
            }
            if(index == myNames.length - 2) {
                sb.append("and ");
            }
        }
        return sb.toString();

        [alex, alberto, juan]
        alex, alberto, juan,
        // quitar ultima coma
        //peunultimo cambiar , por , and
        alex, alberto, and juan

        foreach
         nombre + " ,"

// me voy a la ultima coma y lo cambio por and

    }
}
