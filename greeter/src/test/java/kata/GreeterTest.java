package kata;

import kata.Greeter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


public class GreeterTest {
    private Console console;
    private Greeter sut;

    //    @Test
//    public void test_Greeter_Greet() {
//        // Setup -> A Arrange -> Given
//        Greeter greeter = new Greeter();
//
//        // Test ->  A act -> When
//        String myName = "Bob";
//        String result = greeter.greet(myName);
//
//        // Assert -> A assert -> Then
//        String expectedResult = "Hello, " + myName + "!";
//        Assert.assertEquals(expectedResult, result);
//    }

    @Before
    public void setup() {
        console = Mockito.spy(Console.class);
        sut = new Greeter(console);
    }

    @Test
    public void test_GivenAName_WhenGreet_PrintsNamedGreet() {
        // Setup -> A Arrange -> Given

        // Test ->  A act -> When
        String myName = "Bob";
        sut.greet(myName);

        // Assert -> A assert -> Then
        String expectedResult = "Hello, " + myName + "!";
        Mockito.verify(console).print(expectedResult);
    }

    @Test
    public void test_GivenNullName_WhenGreet_PrintsGenericGreet() {
        // Setup -> A Arrange -> Given

        // Test ->  A act -> When
        sut.greet((String)null);

        // Assert -> A assert -> Then
        String expectedResult = "Hello, my friend!";
        Mockito.verify(console).print(expectedResult);
    }

    @Test
    public void test_GivenANameShouting_WhenGreet_PrintsShoutingNamedGreet() {
        // Setup -> A Arrange -> Given

        // Test ->  A act -> When
        String myName = "JERRY";
        sut.greet(myName);

        // Assert -> A assert -> Then
        String expectedResult = "HELLO, " + myName + "!";
        Mockito.verify(console).print(expectedResult);
    }

    @Test
    public void test_GivenTwoNames_WhenGreet_PrintsGreetWithTwoNames() {
        // Setup -> A Arrange -> Given

        // Test ->  A act -> When
        String[] myNames = {"Jill","Jane"};
        sut.greet(myNames);

        // Assert -> A assert -> Then
        String expectedResult = "Hello, " + myNames[0] + " and " + myNames[1] + "!";
        Mockito.verify(console).print(expectedResult);
    }

    @Test
    public void test_GivenThreeNames_WhenGreet_PrintsGreetWithThreeNames() {
        // Setup -> A Arrange -> Given

        // Test ->  A act -> When
        String[] myNames = {"Jill","Jane", "Bob"};
        sut.greet(myNames);

        // Assert -> A assert -> Then
        String expectedResult = "Hello, " + myNames[0] + ", " + myNames[1] + ", and " + myNames[2] + "!";
        Mockito.verify(console).print(expectedResult);
    }
}
